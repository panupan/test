//
//  main.m
//  test
//
//  Created by Phanuphan Fairee on 19/1/57.
//  Copyright (c) พ.ศ. 2557 Phanuphan Fairee. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
